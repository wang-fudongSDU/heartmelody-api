package com.heartmelody.dudui.domain.dto;

import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiOperation("用于单个音乐的上传")
public class MusicInfo {

    private String mname ;
    private Integer duration;
    private String format;
    private Integer size;
    private String address;

}

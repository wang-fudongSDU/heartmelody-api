package com.heartmelody.dudui.controller;

import com.heartmelody.dudui.config.OssConfig;
import com.heartmelody.dudui.domain.dto.MusicInfo;
import com.heartmelody.dudui.mapper.MusicMapper;
import com.heartmelody.dudui.utils.OSSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

@RestController
@RequestMapping("/music")
public class HmMusicCreateController {

    static Path path = Paths.get(System.getProperty("user.dir"),"src/out/");

    @Autowired
    private OssConfig ossConfig;

    @Autowired
    private MusicMapper mapper;


    @PostMapping("/create")
    public Object createMusic(@RequestBody Map<String,Object> mp) throws IOException {
        String desc = (String) mp.get("desc");

        // 调用相关api
        String musicPath = path.toAbsolutePath() + File.separator + System.currentTimeMillis() + ".mp3";

        File musicFile = Files.createFile(Paths.get(musicPath)).toFile();

        // 调用相关api

         //得到相关的音乐文件，上传oss插入库中
        InputStream inputStream = new FileInputStream(musicFile);
        MultipartFile multipartFile = new MockMultipartFile(musicFile.getName(),"","audio/mp3", inputStream);
        String ossPath = System.currentTimeMillis() + "SpiderCreate.webm";
        // 保存到oss阿里云
        String upload = OSSUtil.upload(ossConfig, multipartFile, "scene/scene", ossPath);
        MusicInfo info = new MusicInfo().setMname(musicPath.substring(musicPath.lastIndexOf(File.separator)))
                .setAddress(upload).setDuration(60)
                .setSize(100).setFormat("mp3");
        mapper.insertInstace(info);
        return upload;

    }



}
